package com.example.javi.myapplication.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.example.javi.myapplication.data.ContratoMusica.MusicaEntry;

/**
 * Created by JAvi on 02/05/2018.
 */

public class MusicaProvider extends ContentProvider {

    public static final String LOG_TAG = MusicaProvider.class.getSimpleName();
    private static final int CANCION = 100;

    private static final int CANCION_ID = 101;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(ContratoMusica.CONTENT_AUTHORITY, ContratoMusica.PATH_PERSONAJE, CANCION);
        sUriMatcher.addURI(ContratoMusica.CONTENT_AUTHORITY, ContratoMusica.PATH_PERSONAJE + "/#", CANCION_ID);
    }
    private MusicaHelper mDbHelper;
    @Override
    public boolean onCreate() {
        mDbHelper = new MusicaHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        Cursor cursor;

        // Figure out if the URI matcher can match the URI to a specific code
        int match = sUriMatcher.match(uri);
        switch (match) {
            case CANCION:
                cursor = database.query(MusicaEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case CANCION_ID:
                selection = MusicaEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                cursor = database.query(MusicaEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case CANCION:
                return insertMusica(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    /**
     * Insert a personaje into the database with the given content values. Return the new content URI
     * for that specific row in the database.
     */
    private Uri insertMusica(Uri uri, ContentValues values) {
        // Check that the name is not null
        String name = values.getAsString(MusicaEntry.COLUMN_MUSICA_NOMBRE);
        if (name == null) {
            throw new IllegalArgumentException("Cancion requiere nombre");
        }
        String artista = values.getAsString(MusicaEntry.COLUMN_MUSICA_ARTISTA);
        if (artista == null) {
            throw new IllegalArgumentException("La cancion necesita un artista");
        }
        String direccion = values.getAsString(MusicaEntry.COLUMN_MUSICA_DIRECCION);
        if (direccion == null) {
            throw new IllegalArgumentException("Escriba algo");
        }
        String Categoria = values.getAsString(MusicaEntry.COLUMN_MUSICA_CATEGORIA);
        if (Categoria == null) {
            throw new IllegalArgumentException("Elija categoria");
        }
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long id = database.insert(MusicaEntry.TABLE_NAME, null, values);
        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row for " + uri);
            return null;
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, id);
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection,
                      String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case CANCION:
                return updatePersonaje(uri, contentValues, selection, selectionArgs);
            case CANCION_ID:
                selection = MusicaEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updatePersonaje(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }

    private int updatePersonaje(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // If the {@link PetEntry#COLUMN_PERSONAJE_NOMBRE} key is present,
        // check that the name value is not null.
        if (values.containsKey(MusicaEntry.COLUMN_MUSICA_NOMBRE)) {
            String name = values.getAsString(MusicaEntry.COLUMN_MUSICA_NOMBRE);
            if (name == null) {
                throw new IllegalArgumentException("Cancion requiere nombre");
            }
        }
        if (values.containsKey(MusicaEntry.COLUMN_MUSICA_ARTISTA)) {
            String artista = values.getAsString(MusicaEntry.COLUMN_MUSICA_ARTISTA);
            if (artista == null) {
                throw new IllegalArgumentException("La cancion necesita un artista");
            }
        }
        if (values.containsKey(MusicaEntry.COLUMN_MUSICA_DIRECCION)) {
            String direccion = values.getAsString(MusicaEntry.COLUMN_MUSICA_DIRECCION);
            if (direccion == null) {
                throw new IllegalArgumentException("Escriba algo");
            }
        }
        if (values.containsKey(MusicaEntry.COLUMN_MUSICA_CATEGORIA)) {
            String Categoria = values.getAsString(MusicaEntry.COLUMN_MUSICA_CATEGORIA);
            if (Categoria == null) {
                throw new IllegalArgumentException("Elija categoria");
            }
        }

        if (values.size() == 0) {
            return 0;
        }

        SQLiteDatabase database = mDbHelper.getWritableDatabase();


        int rowsUpdated = database.update(MusicaEntry.TABLE_NAME, values, selection, selectionArgs);

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Get writeable database
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        //nuevo
        // Track the number of rows that were deleted
        int rowsDeleted;

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case CANCION:
                // Delete all rows that match the selection and selection args
                //return database.delete(PersonajeEntry.TABLE_NAME, selection, selectionArgs);
                //nuevo
                // Delete all rows that match the selection and selection args
                rowsDeleted = database.delete(MusicaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CANCION_ID:
                // Delete a single row given by the ID in the URI
                selection = MusicaEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                //return database.delete(PersonajeEntry.TABLE_NAME, selection, selectionArgs);
                //nuevo
                rowsDeleted = database.delete(MusicaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }
        // nuevo:
        // If 1 or more rows were deleted, then notify all listeners that the data at the
        // given URI has changed
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        // Return the number of rows deleted
        return rowsDeleted;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case CANCION:
                return MusicaEntry.CONTENT_LIST_TYPE;
            case CANCION_ID:
                return MusicaEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }
}