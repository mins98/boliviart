package com.example.javi.myapplication.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by JAvi on 24/04/2018.
 */
//esta clase hereda de SQLiteOpenHelper
public class LibroHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME="boliviart.db";
    private static int DATABASE_VERSION=1;
    public LibroHelper(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_LIBRO_TABLE=
                "CREATE TABLE "+ ContratoLibro.LibroEntry.TABLE_NAME+ " ("
                        + ContratoLibro.LibroEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + ContratoLibro.LibroEntry.COLUMN_LIBRO_NOMBRE+ " TEXT NOT NULL, "
                        + ContratoLibro.LibroEntry.COLUMN_LIBRO_PRECIO+"  INTEGER NOT NULL, "
                        + ContratoLibro.LibroEntry.COLUMN_LIBRO_CATEGORIA+" TEXT NOT NULL, "
                        + ContratoLibro.LibroEntry.COLUMN_LIBRO_DIRECCION+"  TEXT NOT NULL);";

        //PARA EJECUTAR LA CONSULTA:
        db.execSQL(SQL_CREATE_LIBRO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
