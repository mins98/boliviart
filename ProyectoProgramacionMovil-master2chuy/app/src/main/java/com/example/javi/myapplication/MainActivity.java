package com.example.javi.myapplication;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.example.javi.myapplication.data.ContratoLibro.LibroEntry;
import com.example.javi.myapplication.data.LibroHelper;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    //nuevo
    private static final int PERSONAJE_LOADER = 0;
    LibroCursorAdapter mCursorAdapter;

    private LibroHelper mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libros_principal);
        // Configurando FAB para abrir Insertar:
        FloatingActionButton fab = findViewById(R.id.fabInsertar);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Insertar.class);
                startActivity(intent);
            }
        });
        // To access our database, we instantiate our subclass of SQLiteOpenHelper
        // and pass the context, which is the current activity.
        mDbHelper=new LibroHelper(this);

        // Find the ListView which will be populated with the personaje data
        ListView personajeListView = findViewById(R.id.list);

        // Find and set empty view on the ListView, so that it only shows when the list has 0 items.
        View emptyView = findViewById(R.id.empty_view);
        personajeListView.setEmptyView(emptyView);

        //nuevo
        mCursorAdapter=new LibroCursorAdapter(this,null);
        personajeListView.setAdapter(mCursorAdapter);
        //Nuevo
        getLoaderManager().initLoader(PERSONAJE_LOADER,null,this);

        personajeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent=new Intent(MainActivity.this,Insertar.class);
                Uri currentPersonajeUri = ContentUris.withAppendedId(LibroEntry.CONTENT_URI, id);
                intent.setData(currentPersonajeUri);
                startActivity(intent);
            }
        });

    }


    //Metodo para insertar datos a la tabla
    public void datosPrueba(){

        SQLiteDatabase db=mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(LibroEntry.COLUMN_LIBRO_NOMBRE,"Planeta de cristal");
        values.put(LibroEntry.COLUMN_LIBRO_PRECIO,100);
        values.put(LibroEntry.COLUMN_LIBRO_CATEGORIA,"Ficcion");
        values.put(LibroEntry.COLUMN_LIBRO_DIRECCION,"abcd");

        /*
        //Forma antigua
        long newRowId=db.insert(PersonajeEntry.TABLE_NAME,null,values);
        */
        Uri newUri = getContentResolver().insert(LibroEntry.CONTENT_URI, values);


        //Metodo para mostrar el numero de filas en la tabla personaje en el text view
        displayDatabaseInfo();
        //Metodo para mostrar la consulta en el text view "etConsulta"
        //displayConsulta();
    }
    private void displayDatabaseInfo() {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor;
        cursor = db.rawQuery("SELECT * FROM " + LibroEntry.TABLE_NAME, null);
        try {
            // Display the number of rows in the Cursor (which reflects the number of rows in the
            // pets table in the database).
            TextView displayView = (TextView) findViewById(R.id.tvTexto);
            displayView.setText("Numero total de libros: " + cursor.getCount());
        } finally {
            // Always close the cursor when you're done reading from it. This releases all its
            // resources and makes it invalid.
            cursor.close();
        }
    }

    //Metodo para abrir otra activity
    public void insertarDatos(View v){
        Intent intent = new Intent(this, Insertar.class);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_catalog.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Insert dummy data" menu option
            case R.id.accion_insertar_datos_ejemplo:
                datosPrueba();
                displayDatabaseInfo();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        // Definir una proyeccion que especifique cuales columnas desde la BD
        // se debeeria usar en la consulta

        String[] projection = {
                LibroEntry._ID,
                LibroEntry.COLUMN_LIBRO_NOMBRE,
                LibroEntry.COLUMN_LIBRO_CATEGORIA };

        // Realizando la consulta en la tabla personaje
        return new CursorLoader(this,
                LibroEntry.CONTENT_URI,   // Nombre de la tabla de la consulta
                projection,                 // Las columnas a retornar
                null,                  // No selection clause
                null,                  // No selection arguments
                null);                   // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        //update {@link LibroCursorAdapter} with this new cursor containing update personaje data
        mCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        //Callback called when the data needs to be deleted
        mCursorAdapter.swapCursor(null);

    }
}
