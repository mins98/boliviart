package com.example.javi.myapplication.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by JAvi on 24/04/2018.
 */

public class ContratoMusica {
    //crear un constructor por defecto vacio y privado
    private ContratoMusica() {
    }

        public static final String CONTENT_AUTHORITY = "com.example.javi.myapplication";
        public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
        public static final String PATH_PERSONAJE = "musica";
        public static final class MusicaEntry implements BaseColumns {

            public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PERSONAJE);
            public static final String CONTENT_LIST_TYPE =
                    ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERSONAJE;

            public static final String CONTENT_ITEM_TYPE =
                    ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERSONAJE;

            public final static String TABLE_NAME = "musica";
            public final static String _ID = BaseColumns._ID;
            public static final String COLUMN_MUSICA_NOMBRE = "nombre";
            public static final String COLUMN_MUSICA_ARTISTA="artista";
            public static final String COLUMN_MUSICA_CATEGORIA="categoria";
            public static final String COLUMN_MUSICA_DIRECCION="direccion";
        }
}
