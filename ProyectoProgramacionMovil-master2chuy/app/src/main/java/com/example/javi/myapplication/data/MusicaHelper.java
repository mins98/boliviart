package com.example.javi.myapplication.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by JAvi on 24/04/2018.
 */
//esta clase hereda de SQLiteOpenHelper
public class MusicaHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME="boliviart.db";
    private static int DATABASE_VERSION=1;
    //constructor heredado de SQLiteOpenHelper
    public MusicaHelper(Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_LIBRO_TABLE=
                "CREATE TABLE "+ ContratoMusica.MusicaEntry.TABLE_NAME+ " ("
                        + ContratoMusica.MusicaEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + ContratoMusica.MusicaEntry.COLUMN_MUSICA_NOMBRE+ " TEXT NOT NULL, "
                        + ContratoMusica.MusicaEntry.COLUMN_MUSICA_ARTISTA+"  TEXT NOT NULL, "
                        + ContratoMusica.MusicaEntry.COLUMN_MUSICA_CATEGORIA+" TEXT NOT NULL, "
                        + ContratoMusica.MusicaEntry.COLUMN_MUSICA_DIRECCION+"  TEXT NOT NULL);";
        db.execSQL(SQL_CREATE_LIBRO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
