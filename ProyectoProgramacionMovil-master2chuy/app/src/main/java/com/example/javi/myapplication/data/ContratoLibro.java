package com.example.javi.myapplication.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by JAvi on 24/04/2018.
 */

public class ContratoLibro {
    //crear un constructor por defecto vacio y privado
    private ContratoLibro() {
    }

        public static final String CONTENT_AUTHORITY = "com.example.javi.myapplication";
        public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
        public static final String PATH_PERSONAJE = "libro";
        public static final class LibroEntry implements BaseColumns {

            public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PERSONAJE);
            public static final String CONTENT_LIST_TYPE =
                    ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERSONAJE;

            public static final String CONTENT_ITEM_TYPE =
                    ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERSONAJE;

            public final static String TABLE_NAME = "libro";
            public final static String _ID = BaseColumns._ID;
            public static final String COLUMN_LIBRO_NOMBRE = "nombre";
            public static final String COLUMN_LIBRO_PRECIO="precio";
            public static final String COLUMN_LIBRO_CATEGORIA="categoria";
            public static final String COLUMN_LIBRO_DIRECCION="direccion";
        }
}
