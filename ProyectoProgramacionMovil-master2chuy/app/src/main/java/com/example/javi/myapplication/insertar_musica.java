package com.example.javi.myapplication;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.javi.myapplication.data.ContratoLibro;
import com.example.javi.myapplication.data.ContratoMusica;
import com.example.javi.myapplication.data.LibroHelper;
import com.example.javi.myapplication.data.MusicaHelper;

public class insertar_musica extends AppCompatActivity {
    private MusicaHelper mDbHelper;
    //Crear view del activity:
    private EditText nombre;
    private EditText artista;
    private EditText direccion;
    private Spinner categoria;
    String cate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar_musica);
        //nuevo
        Intent intent = getIntent();
        Uri currentUri=intent.getData();
        if (currentUri==null){
            setTitle("Agregar un Libro");
        }else {
            setTitle(getString(R.string.titulo_editar_personaje));
        }
        mDbHelper=new MusicaHelper(this);
        nombre=findViewById(R.id.etNombre2);
        artista=findViewById(R.id.etArtista);
        direccion=findViewById(R.id.etDireccion2);
        categoria=findViewById(R.id.spCategorias2);
        setupSpiner();
    }
    private void setupSpiner() {
        //antes de hacer esto hay que crear EL ARRAY Y LOS STRING

        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout
        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_categoria_options, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner
        categoria.setAdapter(genderSpinnerAdapter);
        categoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                cate = (String) categoria.getSelectedItem();
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    public void insertarPersonaje(){
        String nombreString= nombre.getText().toString().trim();
        String artistaString=artista.getText().toString().trim();
        String direccionString=direccion.getText().toString().trim();
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ContratoMusica.MusicaEntry.COLUMN_MUSICA_NOMBRE, nombreString);
        values.put(ContratoMusica.MusicaEntry.COLUMN_MUSICA_ARTISTA,artistaString);
        values.put(ContratoMusica.MusicaEntry.COLUMN_MUSICA_DIRECCION,direccionString);
        values.put(ContratoMusica.MusicaEntry.COLUMN_MUSICA_CATEGORIA,cate);

        // Insert a new row for Toto into the provider using the ContentResolver.
        // Use the {@link PetEntry#CONTENT_URI} to indicate that we want to insert
        // into the pets database table.
        // Receive the new content URI that will allow us to access Toto's data in the future.
        Uri newUri = getContentResolver().insert(ContratoMusica.MusicaEntry.CONTENT_URI, values);

        if (newUri == null) {
            // If the new content URI is null, then there was an error with insertion.
            Toast.makeText(this, getString(R.string.insercion_personaje_fallo),
                    Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the insertion was successful and we can display a toast.
            Toast.makeText(this, getString(R.string.insercion_personaje_exito),
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_insertar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.accion_guardar:
                // Save pet to database
                insertarPersonaje();
                // Exit activity
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.accion_eliminar:
                // Do nothing for now
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

