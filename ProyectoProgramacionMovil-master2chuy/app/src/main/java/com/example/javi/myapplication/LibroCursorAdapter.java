package com.example.javi.myapplication;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.example.javi.myapplication.data.ContratoLibro.LibroEntry;

/**
 * Created by JAvi on 06/05/2018.
 * * {@link LibroCursorAdapter} is an adapter for a list or grid view
 * that uses a {@link Cursor} of pet data as its data source. This adapter knows
 * how to create list items for each row of pet data in the {@link Cursor}.

 */

public class LibroCursorAdapter extends CursorAdapter {

    public LibroCursorAdapter(Context context, Cursor c) {
        super(context, c,0);
    }



    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Inflate a list item view using the layout specified in list_item.xml
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Find individual views that we want to modify in the list item layout
        TextView nombreTextView = view.findViewById(R.id.tvNombre);
        TextView descripcionTextView = view.findViewById(R.id.tvDescripion);

        // Find the columns of pet attributes that we're interested in
        int nombreColumnIndex = cursor.getColumnIndex(LibroEntry.COLUMN_LIBRO_NOMBRE);
        int descripcionColumnIndex = cursor.getColumnIndex(LibroEntry.COLUMN_LIBRO_CATEGORIA);

        // Read the pet attributes from the Cursor for the current pet
        String personajeNombre = cursor.getString(nombreColumnIndex);
        String personajeDescripcion = cursor.getString(descripcionColumnIndex);

        // Update the TextViews with the attributes for the current pet
        nombreTextView.setText(personajeNombre);
        descripcionTextView.setText(personajeDescripcion);
    }
}
