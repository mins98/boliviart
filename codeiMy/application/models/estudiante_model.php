<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estudiante_model extends CI_Model 
{
	public function listaestudiantes()
	{
	$this->db->select('*');
	$this->db->from('estudiantes');
	return $this->db->get();
	}
	public function recuperarestudiante($idestudiante)
	{
		$this->db->select('*');
		$this->db->from('estudiantes');
		$this->db->where('Idestudiante',$idestudiante);
		return $this->db->get();

	}
	public function modificar($idestudiante,$data)
	{
		$this->db->where('Idestudiante',$idestudiante);
		$this->db->update('estudiantes',$data);

	}
	public function agregar($data)
	{
		$this->db->insert('estudiantes',$data);
		

	}
	public function eliminar($idestudiante)
	{
		$this->db->where('Idestudiante',$idestudiante);
		$this->db->delete('estudiantes');
	}

}
