<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estudiante extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->view('inc/header_view');
		$this->load->model('estudiante_model');
		$this->load->view('inc/footer_view');
	}
	public function index()
	{
		$this->load->view('inc/header_view');
		$listaestudiantes=$this->estudiante_model->listaestudiantes();
		$data['estudiantes']=$listaestudiantes;
		$this->load->view('lista_view',$data);
		$this->load->view('inc/footer_view');
	}
	public function modificar()
	{
		$this->load->view('inc/header_view');
		$idestudiante=$_POST['idestudiante'];
		$data['info']=$this->estudiante_model->recuperarestudiante($idestudiante);
		$this->load->view('mod_estudiente_view',$data);
		$this->load->view('inc/footer_view');
	}
	public function modificarbd()
	{
		$idestudiante=$_POST['idestudiante'];
		$nombre=$_POST['nombre'];
		$apellidos=$_POST['apellidos'];
		$data['nombre']=$nombre;
		$data['apellidos']=$apellidos;
		$this->estudiante_model->modificar($idestudiante,$data);//lama al modelo que ejecuta  el sql
		redirect('estudiante/index','refresh');
	}
	public function agregar()
	{
		$this->load->view('inc/header_view');
		$this->load->view('add_estudiante_view');
		$this->load->view('inc/footer_view');
	}
	public function agregarbd()
	{
		$data['nombre']=$_POST['nombre'];
		//$data['apellidos']=$_POST['nombre'];
		$this->estudiante_model->agregar($data);
		redirect('estudiante/index','refresh');
	}
	public function eliminarbd()
	{
		$idestudiante=$_POST['idestudiante'];
		$this->estudiante_model->eliminar($idestudiante);
		redirect('estudiante/index','refresh');
	}
}
