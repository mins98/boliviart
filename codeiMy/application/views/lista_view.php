<!DOCTYPE html>
<html>
<head>
	<title>Lista de estudiantes</title>
</head>
<body>
<div class="row">
<div class="col-md-10">
	

<a href="<?php echo base_url();?>index.php/estudiante/agregar">
<button class="btn btn-primary"  type="button">Agregar Estudiante</button></a>
	<table class="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Note</th>
                <th>Estado</th>
                <th>Editar</th>
                <th>Eliminar</th>
              </tr>
            </thead>
            <tbody>
            <?php
            foreach ($estudiantes->result() as $row) 
				{
			?>	
				<tr>
                <td><?php echo $row->Idestudiante;?></td>
                <td><?php echo $row->nombre;?></td>
                <td><?php echo $row->apellidos;?></td>
                <td><?php echo $row->nota;?></td>
                <td>
                <?php 
                if($row->nota>50)
                {
                	?>	
                	<span class="label label-success">Aprobado</span>
                <?php 
				}
				else
				{
					?>	
                	<span class="label label-danger">Reprobado</span>
                <?php 
				}

                ?>	
                </td>
                <td><?php echo form_open_multipart('estudiante/modificar');?>
				<input type="hidden" name="idestudiante" value="<?php echo $row->Idestudiante;?>">
				<button class="btn btn-warning" type="submit">Modificar</button>
				<?php echo form_close(); ?></td>
                <td><?php echo form_open_multipart('estudiante/eliminarbd');?>
				<input type="hidden" name="idestudiante" value="<?php echo $row->Idestudiante;?>">
				<button class="btn btn-danger"  type="submit">Eliminar</button>
				<?php echo form_close(); ?></td>
              	</tr>
			<?php
				}
			?>            
            </tbody>
          </table>
          </div>
</div>
<a href="<?php echo base_url();?>index.php/estudiante/agregar">
<button type="button">Agregar Estudiante</button></a>
<ul>

<?php
foreach ($estudiantes->result() as $row) 
{
?>	
<li><?php echo $row->nombre.' '.$row->apellidos;?></li>
<?php echo form_open_multipart('estudiante/modificar');//al controlador estudiante metodo modificar  ?>
<input type="hidden" name="idestudiante" value="<?php echo $row->Idestudiante;?>">
<button type="submit">Modificar</button>
<?php echo form_close(); ?>
<?php echo form_open_multipart('estudiante/eliminarbd');?>
<input type="hidden" name="idestudiante" value="<?php echo $row->Idestudiante;?>">
<button type="submit">Eliminar</button>
<?php echo form_close(); ?>
<?php
}
?>
</ul>
</body>
</html>


